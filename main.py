import time
import os


# Line counter, closes automatically
def file_len(filename):
    with open(filename) as f:
        for i, _ in enumerate(f):
            pass
    return i + 1


# Pass it a top level directory which you want to search for .go files and a counter
def directory_file_parser(current_directory, counter):
    # Walk all the subdirectories and files found in this top directory
    for directoryPath, _, fileNames in os.walk(os.path.normpath(current_directory)):
        for file in fileNames:
            if file.endswith(f'.pas'):
                # Get full path to file; join the directory path and file name
                filenamePath = os.path.join(directoryPath, file)

                # Count lines
                counter += file_len(f'{filenamePath}')
    return counter


if __name__ == f'__main__':
    # alternative to time.process_time
    startTime = time.perf_counter()

    print(f'Starting counter...')
    # Fetch current directory and use it as a top directory in searching go files
    workingDirectory = (os.path.dirname(os.path.abspath(__file__)))

    lineCount = directory_file_parser(workingDirectory, 0)

    print(f'Total number of lines: {lineCount}.')
    print(f'--- {(time.perf_counter() - startTime)} seconds ---')
